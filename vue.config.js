module.exports = {
    lintOnSave: false,
    pluginOptions: {
        electronBuilder: {
            nodeIntegration: true,
            builderOptions: {
                "win": {
                    "target": ["nsis-web", "nsis"],
                    "signAndEditExecutable": true,
                },
                "productName": "Mineboxes_Launcher",
                "appId": "mbl",
                "directories": {
                    "buildResources": "build",
                },
                "extraFiles": [{
                    "from": "runtime/",
                    "to": "runtime/",
                    "filter": ["**/*"]
                }],
                "nsis": {
                    "artifactName": "${productName}_${version}.${ext}",
                    "oneClick": false,
                    "allowToChangeInstallationDirectory": false,
                    "perMachine": false,
                    "differentialPackage": true,
                    "createDesktopShortcut": true,
                    "runAfterFinish": true,
                },
                "nsisWeb": {
                    "appPackageUrl": "https://github.com/KingMihei/mineboxes-lite-launcher/releases/download/v${version}/mineboxeslauncherlite-${version}-x64.nsis.7z",
                    "artifactName": "${productName}_${version}_web.${ext}",
                    "oneClick": false,
                    "allowToChangeInstallationDirectory": false,
                    "perMachine": false,
                    "differentialPackage": true,
                    "createDesktopShortcut": true,
                    "runAfterFinish": true,
                },
                publish: ['github'],
            }
        },
    }
}
