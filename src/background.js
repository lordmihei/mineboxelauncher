'use strict'

import {app, protocol, BrowserWindow, ipcMain} from 'electron';
import {autoUpdater} from "electron-updater"
import {createProtocol} from 'vue-cli-plugin-electron-builder/lib';
import installExtension, {VUEJS_DEVTOOLS} from 'electron-devtools-installer';
import {getVersionList} from "@xmcl/installer";
import fetch from 'node-fetch';
import {machineIdSync} from "node-machine-id";
import Http from "./backend/helpers/http";
import Game, {ProcessController} from '@/backend/helpers/game'
import ForgeParse from '@/backend/helpers/forgeParse'
import selfChecker, {launcherSettings} from "@/backend/helpers/selfChecker";


const guid = machineIdSync();
let userName = '';
const customLog = require("electron-log");
const fs = require("fs");
const fsExtra = require('fs-extra');
const isOnline = require('is-online');

const _ = require('lodash');
const isDevelopment = process.env.NODE_ENV !== 'production'

// Global Vars
import {
    //------Main Data
    launcherSettingsPath, mcFolderPath,
    //------Java
    globalJavaPath,
} from '@/backend/helpers/globalVars'

console.debug('javaPath', globalJavaPath);

//------Connection Data
let connectionState;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
    {scheme: 'app', privileges: {secure: true, standard: true}}
])


let win;

async function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 1050,
        height: 650,
        frame: false, //ОТКЛЮЧЕНИЕ РАМКИ ОКНА
        resizable: false,
        show: false,
        title: 'Mineboxes',
        icon: __dirname + '/src/assets/minecraft_logo_icon_168974.ico',
        webPreferences: {

            // Use pluginOptions.nodeIntegration, leave this alone
            // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
            nodeIntegration: true,
            contextIsolation: false,
        }
    });
    console.debug('dirname', __dirname);

    // Connection state block
    connectionState = await isOnline();
    let lastConnectionState = connectionState;
    console.debug('connectionState == ' + connectionState);


    let timeout = 5000;
    setInterval(() => {
        if (connectionState === false) {
            timeout = 5000;
        } else if (connectionState === true) {
            timeout = 15000;
        }
    }, 1000)
    setInterval(async () => {
        connectionState = await isOnline();
        if (lastConnectionState !== connectionState) {
            console.debug('connectionState == ' + connectionState);
        }
        lastConnectionState = connectionState;
    }, timeout)

    win.setMenuBarVisibility(false);
    if (process.env.WEBPACK_DEV_SERVER_URL) {
        // Load the url of the dev server if in development mode
        await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
        if (!process.env.IS_TEST) win.webContents.openDevTools()
    } else {
        createProtocol('app')
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
    }
    win.once('ready-to-show', () => {
        win.show();
    });
}


// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    if (isDevelopment && !process.env.IS_TEST) {
        // Install Vue Devtools
        try {
            await installExtension(VUEJS_DEVTOOLS)
        } catch (e) {
            console.error('Vue Devtools failed to install:', e.toString())
        }
    }
    createWindow();
    autoUpdater.setFeedURL({
        provider: 'github',
        repo: 'mineboxes-lite-launcher',
        owner: 'kingmihei',
    });
    autoUpdater.logger = customLog;
    autoUpdater.checkForUpdatesAndNotify();

    selfChecker.checkFiles();
    await selfChecker.getServers();

    //
    //
    // let serverList = false;
    // let launcherSettings = {
    //     lastVersion: null,
    //     lastUsername: null,
    //     installedVersions: [],
    //     installedForgeVersions: [],
    //     adServersMain: [],
    //     adServersDat: [],
    //     userSavedServers: [],
    //     userFavoriteServers: [],
    // };
    //
    // const http = new Http();
    //
    // if (!fs.existsSync(mcFolderPath)) {
    //     fs.mkdirSync(mcFolderPath);
    // }
    //
    // if (!fs.existsSync(launcherSettingsPath)) {
    //     fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
    // } else {
    //     let launcherSettingsRaw = fs.readFileSync(launcherSettingsPath, 'utf-8');
    //     launcherSettingsRaw = launcherSettingsRaw.replace(/\\n/g, "\\n")
    //         .replace(/\\'/g, "\\'")
    //         .replace(/\\"/g, '\\"')
    //         .replace(/\\&/g, "\\&")
    //         .replace(/\\r/g, "\\r")
    //         .replace(/\\t/g, "\\t")
    //         .replace(/\\b/g, "\\b")
    //         .replace(/\\f/g, "\\f");
    //     launcherSettingsRaw = launcherSettingsRaw.replace(/[\u0000-\u0019]+/g, "");
    //     launcherSettingsRaw = launcherSettingsRaw.replace(/^\s+|\s+$/g, "");
    //     launcherSettings = JSON.parse(launcherSettingsRaw);
    //     //проверка установленных версий в dat файле на фактические версии и удаление
    //     launcherSettings.installedVersions.forEach(version => {
    //         if (!fs.existsSync(mcFolderPath + '\\versions\\' + version.id)) {
    //             const elIndex = launcherSettings.installedVersions.indexOf(version.id);
    //             launcherSettings.installedVersions.splice(elIndex, 1);
    //         }//
    //     });
    //     fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');//
    // }
    //
    // http.getServerList().then(data => {
    //     serverList = data;
    //     launcherSettings.adServersMain = serverList;
    //     if (fs.existsSync(mcFolderPath + '\\servers.dat')) {
    //         let servers = fs.readFileSync(mcFolderPath + '\\servers.dat')
    //         readInfo(servers).then(data => {
    //             launcherSettings.adServersMain.forEach((i) => {
    //                 let serverDat = data.find(s => s.ip == (i.ip + ":" + i.port));
    //                 if (!serverDat) {
    //                     data.push({ip: i.ip + ':' + i.port, name: i.name});
    //                 }
    //             })
    //             writeInfo(data).then(data => {
    //                 fs.writeFileSync(mcFolderPath + '\\servers.dat', data)
    //             })
    //         }).catch(e => {
    //             console.error(e);
    //         });
    //     } else {
    //         const data = [];
    //         launcherSettings.adServersMain.forEach((i) => {
    //             data.push({ip: i.ip + ':' + i.port, name: i.name});
    //         })
    //         writeInfo(data).then(data => {
    //             fs.writeFileSync(mcFolderPath + '\\servers.dat', data)
    //         })
    //     }
    //     fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
    // }).catch(e => {
    //     console.error(e);
    //     customLog.error('Error while get serverlist', e);
    // });

    // ОТПРАВКА USERNAME В HTTP.JS

    userName = launcherSettings.lastUsername;
    // Http.testPlayerName({userName: userName});
    Http.postClientAction({action: 'open-launcher', userName: userName, guid: guid}); // передаём в http объект params

    ipcMain.on('check-events', (event, arg) => {
        event.reply('check-events', {action: 'check-connection', result: connectionState});
    })

    // Forge
    // TODO
    // Оставить Forge natives и version.json (При удалении)

    ipcMain.on('forge-events', (event, arg) => {
        if (arg.action == 'open-forge') {
            // Проверка наличия installedForgeVersions в launcherSettings
            if (launcherSettings.installedForgeVersions == undefined) {
                launcherSettings.installedForgeVersions = [];
                fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
            }
            Game.startForge(event, arg, 0, connectionState, launcherSettings);
            console.log('JSON', JSON.stringify(launcherSettings.installedForgeVersions));
        }
    })
    // Vanilla
    ipcMain.on('mc-events', (event, arg) => {
        console.debug(arg);
        if (arg.action == 'open-mc') {
            if (connectionState == true) {
                getVersionList().then((data) => {

                    Game.start(event, arg, data, 0, connectionState, launcherSettings);
                }).catch((e) => {
                    event.reply('service-events', {action: 'error-log', data: e});
                    console.log(e);
                });
            } else if (connectionState == false) {
                Game.start(event, arg, 0, 0, connectionState, launcherSettings);
            }//
        } else if (arg.action == 'open-server') {
            fetch('http://lordmihei.myjino.ru/serverclick/', {
                method: 'POST',
                body: JSON.stringify({is_it_Mihei: 'oh, yes', server: arg.id}),
            }).then(data => {
                // console.debug(data);
            }).catch(e => {
                console.error(e);
            });
            getVersionList().then((data) => {
                Game.start(event, arg, data, 1, connectionState, launcherSettings);
            }).catch((e) => {
                event.reply('service-events', {action: 'error-log', data: e});
                console.log(e);
            });
        } else if (arg.action == 'get-mc-versions') {
            console.debug('getting versions');
            // Есть подключение к сети
            if (connectionState == true) {
                getVersionList().then((data) => {
                    event.reply('mc-events', {action: 'versions', data: data})
                }).catch(e => {
                    event.reply('service-events', {action: 'error-log', data: e});
                    console.debug(e);
                });
            }
            // Нет подключения к сети
            else if (connectionState === false || connectionState === undefined) {
                let launcherSettingsRaw = fs.readFileSync(launcherSettingsPath, 'utf-8');
                launcherSettingsRaw = launcherSettingsRaw.replace(/\\n/g, "\\n")
                    .replace(/\\'/g, "\\'")
                    .replace(/\\"/g, '\\"')
                    .replace(/\\&/g, "\\&")
                    .replace(/\\r/g, "\\r")
                    .replace(/\\t/g, "\\t")
                    .replace(/\\b/g, "\\b")
                    .replace(/\\f/g, "\\f");
                launcherSettingsRaw = launcherSettingsRaw.replace(/[\u0000-\u0019]+/g, "");
                launcherSettingsRaw = launcherSettingsRaw.replace(/^\s+|\s+$/g, "");
                launcherSettings = JSON.parse(launcherSettingsRaw);

                event.reply('mc-events', {action: 'installed-versions', data: launcherSettings.installedVersions});
            }


        } else if (arg.action == 'get-forge-versions') {
            // // Отправляет на клиент список версий
            console.debug('getting forge versions')
            if (connectionState === true) {
                let forgeList = ForgeParse.getData();
                event.reply('mc-events', {action: 'forge-versions', data: forgeList});
            }
            // Offline
            else if (connectionState == false || connectionState == undefined) {
                event.reply('mc-events', {action: 'forge-versions', data: launcherSettings.installedForgeVersions});
            }


        } else if (arg.action == 'chk-installed-versions') {
            console.debug(fs.existsSync(mcFolderPath.getVersionRoot(arg.version)));
        }
    });
    ipcMain.on('service-events', (event, arg) => {
        console.debug('debil', arg);
        if (arg.action == 'get-launcher-settings') {
            event.reply('service-events', {action: 'get-launcher-settings', data: launcherSettings});
        } else if (arg.action == 'set-launcher-settings') {
            if(arg.lastVersion){
                launcherSettings.lastVersion = arg.lastVersion;
            }
            if(arg.lastUsername) {
                launcherSettings.lastUsername = arg.lastUsername;
            }
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        } else if (arg.action == 'set-Forgelauncher-settings') {
            launcherSettings.lastVersion = arg.lastVersion;
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        } else if (arg.action == 'hide-launcher') {
            win.minimize();
        } else if (arg.action == 'close-launcher') {
            console.debug('close');
            win.destroy();
        } else if (arg.action == 'open-shop') {
            const shopWin = new BrowserWindow({
                width: 1050,
                height: 650,
                frame: true,
                resizable: false,
                webPreferences: {

                    // Use pluginOptions.nodeIntegration, leave this alone//
                    // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
                    nodeIntegration: true,
                    contextIsolation: false,
                }
            });
            shopWin.setMenuBarVisibility(false);
            shopWin.loadURL(arg.data.url);
            shopWin.once('ready-to-show', () => {
                shopWin.show();
            })

        } else if (arg.action == 'open-mods-folder') {
            ProcessController.openExplorer('mods')
        } else if (arg.action == 'open-res-folder') {
            ProcessController.openExplorer('resourcepacks')
        }
    });
})
// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', (data) => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}
