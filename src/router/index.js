import Vue from 'vue'
import VueRouter from 'vue-router'
import mainPage from '../pages/mainPage/index';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'mainPage',
    component: mainPage
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
