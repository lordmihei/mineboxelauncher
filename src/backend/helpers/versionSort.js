let separateMin = [".0", ".1", ".2", ".3", ".4", ".5", ".6", ".7", ".8", ".9"];
let separateLow = [".0.", ".1.", ".2.", ".3.", ".4.", ".5.", ".6.", ".7.", ".8.", ".9."];
let separateMedium = [".10", ".11", ".12", ".13", ".14", ".15", ".16", ".17", ".18", ".19", ".20", ".21", ".22", ".23", ".24", ".25", ".26", ".27", ".28", ".29", ".30"];
let separateHigh = [".10.", ".11.", ".12.", ".13.", ".14.", ".15.", ".16.", ".17.", ".18.", ".19.", ".20.", ".21.", ".22.", ".23.", ".24.", ".25.", ".26.", ".27.", ".28.", ".29.", ".30."];

/**
 * Корректно сортирует список версий по убыванию.
 * Группирует снапшоты отдельно.
 * @param Arr
 * @param type - 1 (Forge) || 0 (Vanilla)
 */
export async function versionSort(Arr, type) {
    if (type === 0) {
        let alpha;
        let beta;
        let checkedAlpha;
        let checkedBeta;

        let toReturn;

        await Arr.sort((a, b) => {
            checkedAlpha = false;
            checkedBeta = false;

            // Сортировка с учётом '-pre', '-rc', ' Pre-Release '
            // Первый указатель
            if (a.id.includes('-pre')) {
                alpha = a.id.replace('-pre', '');
            } else if (a.id.includes('-rc')) {
                alpha = a.id.replace('-rc', '');
            } else if (a.id.includes(' Pre-Release ')) {
                alpha = a.id.replace(' Pre-Release ', '');
            }
            // Второй указатель
            if (b.id.includes('-pre')) {
                alpha = b.id.replace('-pre', '');
            } else if (b.id.includes('-rc')) {
                alpha = b.id.replace('-rc', '');
            } else if (b.id.includes(' Pre-Release ')) {
                alpha = b.id.replace(' Pre-Release ', '');
            }
            // Сортировка Snapshot
            // Первый указатель
            if (a.id.includes('w')) {
                alpha = a.id.slice(0, 2) + a.id.slice(3, 5);
                checkedAlpha = true;
            }
            // Второй указатель
            if (b.id.includes('w')) {
                beta = b.id.slice(0, 2) + b.id.slice(3, 5);
                checkedBeta = true;
            }

            // Стандартная сортировка
            // Первый указатель
            for (let q = 0; q < separateLow.length && checkedAlpha !== true; q++) { // include separateLow 0.0.0
                if (a.id.includes(separateLow[q])) {
                    let after = a.id.slice(3);
                    a.id = a.id.slice(0, 3) + a.id.slice(4);
                    alpha = Math.round(parseFloat(a.id) * 100 - 100);
                    a.id = a.id.slice(0, 3) + after;
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateHigh.length && checkedAlpha !== true; q++) { // include separateHigh 0.00.0
                if (a.id.includes(separateHigh[q])) {
                    let after = a.id.slice(4);
                    a.id = a.id.slice(0, 4) + a.id.slice(5);
                    alpha = Math.round(parseFloat(a.id) * 1000 - 1000);
                    a.id = a.id.slice(0, 4) + after;
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateMedium.length && checkedAlpha !== true; q++) { // include separateMedium 0.00
                if (a.id.includes(separateMedium[q])) {
                    alpha = Math.round(parseFloat(a.id) * 1000 - 1000);
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateMin.length && checkedAlpha !== true; q++) { // include separateMin 0.0
                if (a.id.includes(separateMin[q])) {
                    alpha = parseFloat(a.id) * 10 - 10;
                    checkedAlpha = true;
                    break
                }
            }

            // Второй указатель
            for (let w = 0; w < separateLow.length && checkedBeta !== true; w++) { // include separateLow 0.0.0
                if (b.id.includes(separateLow[w])) {
                    let after = b.id.slice(3)
                    b.id = b.id.slice(0, 3) + b.id.slice(4);
                    beta = Math.round(parseFloat(b.id) * 100 - 100);
                    b.id = b.id.slice(0, 3) + after;
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateHigh.length && checkedBeta !== true; w++) { // include separateHigh 0.00.0
                if (b.id.includes(separateHigh[w])) {
                    let after = b.id.slice(4);
                    b.id = b.id.slice(0, 4) + b.id.slice(5);
                    beta = Math.round(parseFloat(b.id) * 1000 - 1000);
                    b.id = b.id.slice(0, 4) + after;
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateMedium.length && checkedBeta !== true; w++) { // include separateMedium 0.00
                if (b.id.includes(separateMedium[w])) {
                    beta = Math.round(parseFloat(b.id) * 1000 - 1000);
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateMin.length && checkedBeta !== true; w++) { // include separateMin 0.0
                if (b.id.includes(separateMin[w])) {
                    beta = parseFloat(b.id) * 10 - 10;
                    checkedBeta = true;
                    break
                }
            }

            if (a !== undefined && b !== undefined) {
                if (alpha < beta) {
                    toReturn = 1;
                } else {
                    toReturn = -1;
                }
            }

            return toReturn;
        })
    } else if (type === 1) {
        await Arr.sort((a, b) => {
            let alpha;
            let beta;
            let checkedAlpha;
            let checkedBeta;

            let toReturn;

            // Стандартная сортировка
            // Первый указатель
            for (let q = 0; q < separateLow.length && checkedAlpha !== true; q++) { // include separateLow 0.0.0
                if (a.mcversion.includes(separateLow[q])) {
                    let after = a.mcversion.slice(3);
                    a.mcversion = a.mcversion.slice(0, 3) + a.mcversion.slice(4);
                    alpha = Math.round(parseFloat(a.mcversion) * 100 - 100);
                    a.mcversion = a.mcversion.slice(0, 3) + after;
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateHigh.length && checkedAlpha !== true; q++) { // include separateHigh 0.00.0
                if (a.mcversion.includes(separateHigh[q])) {
                    let after = a.mcversion.slice(4);
                    a.mcversion = a.mcversion.slice(0, 4) + a.mcversion.slice(5);
                    alpha = Math.round(parseFloat(a.mcversion) * 1000 - 1000);
                    a.mcversion = a.mcversion.slice(0, 4) + after;
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateMedium.length && checkedAlpha !== true; q++) { // include separateMedium 0.00
                if (a.mcversion.includes(separateMedium[q])) {
                    alpha = Math.round(parseFloat(a.mcversion) * 1000 - 1000);
                    checkedAlpha = true;
                    break
                }
            }
            for (let q = 0; q < separateMin.length && checkedAlpha !== true; q++) { // include separateMin 0.0
                if (a.mcversion.includes(separateMin[q])) {
                    alpha = parseFloat(a.mcversion) * 10 - 10;
                    checkedAlpha = true;
                    break
                }
            }

            // Второй указатель
            for (let w = 0; w < separateLow.length && checkedBeta !== true; w++) { // include separateLow 0.0.0
                if (b.mcversion.includes(separateLow[w])) {
                    let after = b.mcversion.slice(3)
                    b.mcversion = b.mcversion.slice(0, 3) + b.mcversion.slice(4);
                    beta = Math.round(parseFloat(b.mcversion) * 100 - 100);
                    b.mcversion = b.mcversion.slice(0, 3) + after;
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateHigh.length && checkedBeta !== true; w++) { // include separateHigh 0.00.0
                if (b.mcversion.includes(separateHigh[w])) {
                    let after = b.mcversion.slice(4);
                    b.mcversion = b.mcversion.slice(0, 4) + b.mcversion.slice(5);
                    beta = Math.round(parseFloat(b.mcversion) * 1000 - 1000);
                    b.mcversion = b.mcversion.slice(0, 4) + after;
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateMedium.length && checkedBeta !== true; w++) { // include separateMedium 0.00
                if (b.mcversion.includes(separateMedium[w])) {
                    beta = Math.round(parseFloat(b.mcversion) * 1000 - 1000);
                    checkedBeta = true;
                    break
                }
            }
            for (let w = 0; w < separateMin.length && checkedBeta !== true; w++) { // include separateMin 0.0
                if (b.mcversion.includes(separateMin[w])) {
                    beta = parseFloat(b.mcversion) * 10 - 10;
                    checkedBeta = true;
                    break
                }
            }
            if (a !== undefined && b !== undefined) {
                if (alpha < beta) {
                    toReturn = 1;
                } else {
                    toReturn = -1;
                }
            }
            return toReturn;
        })
    }

    console.log('Список рассортирован: ');
    console.log(Arr);
}