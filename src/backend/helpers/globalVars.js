import {javaDetect} from "@/backend/helpers/javaDetect";

// GLOBAL USABLE VARIABLES
//------Main Data
export const launcherName = 'Mineboxes_Launcher';
export const gameFolderName = 'mineboxes';
export const mcFolderPath = process.env.APPDATA + '\\' + gameFolderName;
export const launcherSettingsPath = mcFolderPath + '\\mblConfig.dat';

//------Java Data
export const globalJavaPath = javaDetect(launcherName);

