import {launcherSettingsPath, mcFolderPath} from "@/backend/helpers/globalVars";
import {readInfo, writeInfo} from "@xmcl/server-info";
import Http from "@/backend/helpers/http";

const fs = require("fs");
const customLog = require("electron-log");

export let launcherSettings = {
    lastVersion: null,
    lastUsername: null,
    installedVersions: [],
    installedForgeVersions: [],
    adServersMain: [],
    adServersDat: [],
    userSavedServers: [],
    userFavoriteServers: [],
};

class selfChecker {
    constructor() {
        this.serverList = false;
    }

    /**
     * Проверка наличия корневой папки с игрой и файла launcherSettings
     */
    async checkFiles() {
        if (!fs.existsSync(mcFolderPath)) {
            fs.mkdirSync(mcFolderPath);
        }

        if (!fs.existsSync(launcherSettingsPath)) {
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        }
        else {
            let launcherSettingsRaw = fs.readFileSync(launcherSettingsPath, 'utf-8');
            launcherSettingsRaw = launcherSettingsRaw.replace(/\\n/g, "\\n")
                .replace(/\\'/g, "\\'")
                .replace(/\\"/g, '\\"')
                .replace(/\\&/g, "\\&")
                .replace(/\\r/g, "\\r")
                .replace(/\\t/g, "\\t")
                .replace(/\\b/g, "\\b")
                .replace(/\\f/g, "\\f");
            // eslint-disable-next-line no-control-regex
            launcherSettingsRaw = launcherSettingsRaw.replace(/[\u0000-\u0019]+/g, "");
            launcherSettingsRaw = launcherSettingsRaw.replace(/^\s+|\s+$/g, "");
            launcherSettings = JSON.parse(launcherSettingsRaw);
            //проверка установленных версий в dat файле на фактические версии и удаление
            launcherSettings.installedVersions.forEach(version => {
                if (!fs.existsSync(mcFolderPath + '\\versions\\' + version.id)) {
                    const elIndex = launcherSettings.installedVersions.indexOf(version.id);
                    launcherSettings.installedVersions.splice(elIndex, 1);
                }//
            });
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');//
        }
    }

    /**
     * Получение списка серверов и запись их в launcherSettings
     */
    async getServers() {
        Http.getServerList().then(data => {
            this.serverList = data;
            launcherSettings.adServersMain = this.serverList;
            if (fs.existsSync(mcFolderPath + '\\servers.dat')) {
                let servers = fs.readFileSync(mcFolderPath + '\\servers.dat')
                readInfo(servers).then(data => {
                    launcherSettings.adServersMain.forEach((i) => {
                        let serverDat = data.find(s => s.ip == (i.ip + ":" + i.port));
                        if (!serverDat) {
                            data.push({ip: i.ip + ':' + i.port, name: i.name});
                        }
                    })
                    writeInfo(data).then(data => {
                        fs.writeFileSync(mcFolderPath + '\\servers.dat', data)
                    })
                }).catch(e => {
                    console.error(e);
                });
            } else {
                const data = [];
                launcherSettings.adServersMain.forEach((i) => {
                    data.push({ip: i.ip + ':' + i.port, name: i.name});
                })
                writeInfo(data).then(data => {
                    fs.writeFileSync(mcFolderPath + '\\servers.dat', data)
                })
            }
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        }).catch(e => {
            console.error(e);
            customLog.error('Error while get serverlist', e);
        });
    }
}

export default new selfChecker();