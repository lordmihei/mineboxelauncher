'use strict'

import fetch from "node-fetch";

const customLog = require("electron-log");

class Http {

    constructor() {
        this.url = 'http://lordmihei.myjino.ru/';
    }
// ПРОВЕРКА ПОЛУЧЕНИЯ НИКА
//     async testPlayerName() {
//         await console.debug('Username:', userName);
//     }

    async getServerList() {
        let response = await fetch(this.url + 'serverlist/').catch(error => {
            customLog.error('Error while getting serverlist', error);
        });
        response = await response.json();
        return response;
    }

    // @params {тип объекта для удобства} указание ключа объекта.

    /**
     * Функция для отправки действий пользователя на конфигурационный сервер
     * @param {Object} params
     * @param {String} params.action - название действия
     * @param {String} params.guid - уникальный идентификатор пользователя
     * @param {String} params.nickname - имя пользователя
     * @returns {Promise<response>}
     */
    async postClientAction(params) { // Отправка с background объекта params

        if (params.action == 'open-launcher') {
            const response = await fetch(this.url + 'player/client_action/', {
                method: 'POST', // отправка данных на сервак**
                body: JSON.stringify({guid: params.guid, nickname: params.userName, action: 'Open launcher'}),
            }).catch(error => {
                customLog.error('Error while post client data', error);
            });
            return response;
        }
    }
}
export default new Http();
