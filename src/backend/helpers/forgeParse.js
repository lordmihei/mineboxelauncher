// // Создание хранилища forge версий.
// function forgeWriteFile() {
//     if (!fs.existsSync(forgeFilePath)) {
//         fs.writeFileSync(forgeFilePath, readyForgeVersions);
//     }
//     if (isUpdateArray === true) {
//         fs.writeFileSync(forgeFilePath, JSON.stringify(readyForgeVersions));
//         console.info('Forge:', 'Список версий обновлён');
//     } else {
//         console.info('Forge:', 'Обновление списка не требуется');
//     }
// }
//
// if (isUpdateArray === true) {
//     sendData();
//
//     // Парсинг списка версий Forge
//     async function createForgeList() {
//         console.debug('getting forge versions');
//         const mcVersions = await getVersionList();
//         for (let mcVersion of mcVersions.versions) {
//             if (mcVersion.type == 'release') {
//                 // console.debug('mcVersion', mcVersion); // вывод всех версий Forge в консоль
//                 let forgeVersions = [];
//                 try {
//                     forgeVersions = await getForgeVersionList({mcversion: mcVersion.id});
//                 } catch (e) {
//                     continue;
//                 }
//                 forgeVersions.versions.forEach(forgeVersion => {
//                     if (forgeVersion.type == 'latest' || forgeVersion.type == 'recommended') {
//                         console.debug('found ' + forgeVersion.type, forgeVersion);
//                         readyForgeVersions.push(forgeVersion);
//                     }
//                 })
//             }
//         }
//         forgeWriteFile();
//     }
//
//     createForgeList();
//     sendData();
//     console.debug('done with forge versions');
// } else {
//     sendData();
// }

class ForgeParse {
    constructor() {
        this.data = [
            {
                "version": "36.1.0",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "f258e5e10621e6462281285e05123340",
                    "sha1": "ea96f65bc415a34709934aed62972934750ad180",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.5-36.1.0/forge-1.16.5-36.1.0-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "ace24a5bfc910af72de81a51dbbf7897",
                    "sha1": "1db69612f1f9ee9eef279f63c6e52db8fe254bb1",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.5-36.1.0/forge-1.16.5-36.1.0-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "c4247eb4064a306402c24d2c6e13f2ab",
                    "sha1": "a4fec6b3ecc5a36e2b10ce4eeca48fea31b74fb0",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.5-36.1.0/forge-1.16.5-36.1.0-mdk.zip"
                },
                "mcversion": "1.16.5",
                "type": "recommended"
            },
            {
                "version": "35.1.4",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "cc3e1dd7f59f5300918a892c6ca54328",
                    "sha1": "9ef92725fd36ccb3d74b3f947e9231eed4093aea",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.4-35.1.4/forge-1.16.4-35.1.4-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "6ceb243ce1c001be246e480ef2700394",
                    "sha1": "a5b25c2a7f4d50d26c794aa0c851249031eb3e5f",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.4-35.1.4/forge-1.16.4-35.1.4-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "8351771aaa9553cf777818741203e460",
                    "sha1": "961b9739a4a2c80dcc48d6391bc4e0a7a6b90f8d",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.4-35.1.4/forge-1.16.4-35.1.4-mdk.zip"
                },
                "mcversion": "1.16.4",
                "type": "recommended"
            },
            {
                "version": "34.1.0",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "360c8230068166d1320f42d80fdc8a4d",
                    "sha1": "7967e8165c14dc6b3a4c0bc13619e49543e373af",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.3-34.1.0/forge-1.16.3-34.1.0-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "ee3b4ed111ad143cfc757aad6dc61024",
                    "sha1": "ac4d4982eb75b0e2adfeebe5b1f398f8bff501b5",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.3-34.1.0/forge-1.16.3-34.1.0-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "e4c3278ac8e26960beb4ca8455ec0214",
                    "sha1": "9d620f917bfbe423bd362f39a0631bea028e0271",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.3-34.1.0/forge-1.16.3-34.1.0-mdk.zip"
                },
                "mcversion": "1.16.3",
                "type": "recommended"
            },
            {
                "version": "33.0.61",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "5cf733e5b73b532f7ec458e3dc35b76b",
                    "sha1": "47357dc0828744e6a7c2f3828dc1be29861b934d",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.2-33.0.61/forge-1.16.2-33.0.61-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "eed904b7c2716fa23eb2964540d63a2a",
                    "sha1": "025d359a7c5b4869c58854c8af04f00561ec1b8e",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.2-33.0.61/forge-1.16.2-33.0.61-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "7ffaef3261f4efe02faf2b73d4c198de",
                    "sha1": "26830cdfd33662e917e7bed572472573a6e3dac0",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.2-33.0.61/forge-1.16.2-33.0.61-mdk.zip"
                },
                "mcversion": "1.16.2",
                "type": "latest"
            },
            {
                "version": "32.0.108",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "bd27629774a6f8b0334a9ed3df7158b4",
                    "sha1": "ea8e1a1903b68bdb3f9d2363599522e69142807b",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.1-32.0.108/forge-1.16.1-32.0.108-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "cbba8a8a7928af1802e1b9f84e4b8ffd",
                    "sha1": "d2c22d4c1d61d6126cb15a9a087da2ef57e7acdf",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.1-32.0.108/forge-1.16.1-32.0.108-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "2a593b6cea05de45d9abd43e5476ea27",
                    "sha1": "aa707bf3f0fd0d42ef3dbfffd1eba7df0653e163",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.1-32.0.108/forge-1.16.1-32.0.108-mdk.zip"
                },
                "mcversion": "1.16.1",
                "type": "latest"
            },
            {
                "version": "31.2.0",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "00f30ff769803c8ad26fed6dfb6c89b7",
                    "sha1": "7c7056461e28124066623a01cba5774389c9486a",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.2-31.2.0/forge-1.15.2-31.2.0-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "a98337d668debbe47fd6ed1abdffa8da",
                    "sha1": "679ea40cab6e5c9828dd45c7bf326f65b8bc89b7",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.2-31.2.0/forge-1.15.2-31.2.0-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "686cea30e29a1eb490573ae75378a655",
                    "sha1": "89628c432cf7631fcda4d0ea50d3986aaa519c28",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.2-31.2.0/forge-1.15.2-31.2.0-mdk.zip"
                },
                "mcversion": "1.15.2",
                "type": "recommended"
            },
            {
                "version": "30.0.51",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "48d4c9e3cc2b4bef475638647030897f",
                    "sha1": "e323fc436b74b64f41b09f13ebe1bbd77a080936",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.1-30.0.51/forge-1.15.1-30.0.51-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "bdfdd7259c6fa95922498fb40ad5a5d7",
                    "sha1": "9a3e25ce091a32a7ea3f137fbcb8eff45077ed47",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.1-30.0.51/forge-1.15.1-30.0.51-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "ad14b800ceee1c2989ffc864e8f33ebc",
                    "sha1": "8b42d41f6f9f958fc08101fb7203b234b1c133c3",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15.1-30.0.51/forge-1.15.1-30.0.51-mdk.zip"
                },
                "mcversion": "1.15.1",
                "type": "latest"
            },
            {
                "version": "29.0.4",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "bd84679f5994b5b3e41e28471c6b8784",
                    "sha1": "4b4068644b7cacec443a9c1c93eeb3b00e918541",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15-29.0.4/forge-1.15-29.0.4-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "ef817dcb6b58c339c3029231b6c99496",
                    "sha1": "953f5316c678cfe275d13163b8d2b247f4f41cb3",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15-29.0.4/forge-1.15-29.0.4-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "c80f51240bbbd1adcbb7ab29c3cec322",
                    "sha1": "d718753c8ec7112aea4ab3f9c543a5aa9ebd6612",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.15-29.0.4/forge-1.15-29.0.4-mdk.zip"
                },
                "mcversion": "1.15",
                "type": "latest"
            },
            {
                "version": "28.2.0",
                "date": "",
                "changelog": {
                    "name": "changelog",
                    "md5": "36098914b02dc3e5e9ff777c0df84f4b",
                    "sha1": "3bb4c127a02a143a5251efbbc5277f3561bc0a1c",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.14.4-28.2.0/forge-1.14.4-28.2.0-changelog.txt"
                },
                "installer": {
                    "name": "installer",
                    "md5": "a060ecc61df1aa9714c70067ed3cf8d7",
                    "sha1": "020b8461254a9ede7b37a52435934ae61ee93974",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.14.4-28.2.0/forge-1.14.4-28.2.0-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "2306fb047c4f48c2bdfc09b15e191915",
                    "sha1": "0965d9d3752f4472cf03c322d4a399e341318b2e",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.14.4-28.2.0/forge-1.14.4-28.2.0-mdk.zip"
                },
                "mcversion": "1.14.4",
                "type": "recommended"
            },
            {
                "version": "14.23.5.2855",
                "date": "",
                "installer": {
                    "name": "installer",
                    "md5": "b37aedc28e441fec469f910ce913e9c3",
                    "sha1": "f691a3e4d8f46eebb42d6129f5e192bf4e1121d0",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-installer.jar"
                },
                "mdk": {
                    "name": "mdk",
                    "md5": "83543cc09a95d18f402862b418a79075",
                    "sha1": "6e86cd938c364663cf392d0c92ef14fba51a369f",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-mdk.zip"
                },
                "universal": {
                    "name": "universal",
                    "md5": "02fcc447bb8938e5214292e4d36ec949",
                    "sha1": "52cecb6a3fbd765599b976dca445b21f33ab5634",
                    "path": "https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-universal.jar"
                },
                "mcversion": "1.12.2",
                "type": "recommended"
            }]
    }

    /**
     * Возвращает список доступных Forge версий
     * @return {version, mcversion}
     */
    getData() {
        return this.data;
    }

}

export default new ForgeParse();