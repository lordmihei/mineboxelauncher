const fs = require("fs");

// New datE create
let date = new Date();
let today = date.getDate();
const datePath = process.env.APPDATA + '\\mineboxes\\dateFile.dat';


export function dateChecker() {
    try {
        if (!fs.existsSync(datePath)) {
            fs.writeFileSync(datePath, today);
            console.info('Файл Даты успешно создалась');
        }
        const dateFile = fs.readFileSync(datePath);
        if (dateFile == today) { // 0 - это отладка. Заменить на today
            console.info('Актуальная дата');
            return false; // действие не требуется
        } else {
            console.info ('Файл Даты устарел');
            fs.writeFileSync(datePath, today);
            console.info('Дата обновлена');
            return true; // действие необходимо
        }
    }
    catch (e) {
        console.info(e, 'dataChecker', 'Ошибка при создании текущей Даты');
    }
}

/**
 *
 * @param {String | Object} arr - Массив
 * @param {String | Object} key - Ключ
 * @param {String | Object} value - Значение
 * @constructor Array.prototype.indexOf
 * @returns {Promise<number>}
 * Возвращает 1, если найдено value в key. Иначе -1.
 */
export async function ObjIndexOf(arr, key, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i][key] === value) {
            return i;
        }
    }
    return -1;
}
