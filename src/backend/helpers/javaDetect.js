const fs = require("fs");

export function javaDetect(launcherName) {

    if (fs.existsSync(process.env.LOCALAPPDATA + '\\Programs\\' + launcherName + '\\runtime\\Java64\\0+\\bin\\java.exe')) {
        return process.env.LOCALAPPDATA + '\\Programs\\' + launcherName + '\\runtime\\Java64';
    }
    else if (fs.existsSync('C:\\Program Files\\' + launcherName + '\\runtime\\Java64\\0+\\bin\\java.exe')) {
        return 'C:\\Program Files\\' + launcherName + '\\runtime\\Java64';
    }
}