import {launch} from "@xmcl/core";
import {getVersionList, install, installForge, installTask} from "@xmcl/installer";
import {versionSort} from "@/backend/helpers/versionSort";
import {
    gameFolderName,
    globalJavaPath,
    mcFolderPath,
    launcherSettingsPath
} from "@/backend/helpers/globalVars";
import {offline} from "@xmcl/user";

const customLog = require("electron-log");
const fs = require("fs");
const fsExtra = require('fs-extra');
const gamePath = mcFolderPath;

// Process Controller
const exec = require('child_process').execSync;

export class ProcessController {
    constructor() {
        this.justForFun = false;
    }

    static openExplorer(folder) {
        const path = process.env.APPDATA + '\\' + gameFolderName + '\\' + folder;
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        const cmd = 'start ' + path;
        exec(cmd);
    }

    async lockWhileMcRunning(pid) {
        let platform = process.platform;
        let cmd = '';
        switch (platform) {
            case 'win32' :
                cmd = `tasklist`;
                break;
            case 'darwin' :
                cmd = `ps -ax | grep ${query}`;
                break;
            case 'linux' :
                cmd = `ps -A`;
                break;
            default:
                break;
        }
        const timer = ms => new Promise(res => setTimeout(res, ms))
        let isRunning = exec(cmd).toString().indexOf(pid);
        while (isRunning > 0) {
            isRunning = exec(cmd).toString().indexOf(pid);
            await timer(1000);
        }

        return false;
    }
}

const wp = new ProcessController();

class Game {

    constructor() {
    }

    /**
     * Возвращает путь до нужной версии Java (old || new)
     * @param data - {data} from getVersionList()
     * @param arg
     * @param isOnline {boolean}
     * @param launcherSettings
     * @return {string}
     */
    getJavaPath(data, arg, isOnline, launcherSettings) {
        const oldJava = globalJavaPath + '\\0+\\bin\\java.exe';
        const newJava = globalJavaPath + '\\1.17+\\bin\\java.exe'; // **** было jav
        // aw
        if (isOnline === true) {
            const currentFullVersion = data.versions.find(version => version.id == arg.version);
            const anchorVersion = data.versions.find(version => version.id == '21w03a');
            return data.versions.indexOf(currentFullVersion) <= data.versions.indexOf(anchorVersion) ? newJava : oldJava;
        } else if (isOnline === false) {
            const currentFullVersion = arg.version;
            const anchorVersion = '21w03a';
            return launcherSettings.installedVersions.findIndex(version => version.id === currentFullVersion) <= launcherSettings.installedVersions.findIndex(version => version.id === anchorVersion) ? newJava : oldJava;
        }
    }

    /**
     * Запускает игру в нужном режиме, в зависимости от аргументов
     * @param event
     * @param arg
     * @param data - {data} from getVersionList()
     * @param gameType - 1 (Server) || 0 (Offline)
     * @param isOnline {boolean}
     * @param launcherSettings
     */
    launch(event, arg, data, gameType, isOnline, launcherSettings) {
        // event.reply('service-events', {action: 'hide-installing-modal'});
        event.reply('service-events', {action: 'show-launching-modal'});

        const userAuth = (offline(arg.username));
        let javaPath;
        let version;
        if (isOnline === true && !arg.forgeVersion) {
            version = data.versions.find(version => version.id === arg.version);
            version = version.id;
        } else if (isOnline === false) {
            version = arg.version;
        } else if (arg.forgeVersion) {
            version = arg.forgeVersion.mcversion + '-forge-' + arg.forgeVersion.version;
        }
        if (!arg.forgeVersion) {
            javaPath = this.getJavaPath(data, arg, isOnline, launcherSettings);
        } else {
            javaPath = globalJavaPath + '\\0+\\bin\\java.exe';
            console.info('javaPath for FORGE', 'ПОСМОТРИ JAVAPATH, ЕСЛИ УЖЕ ВЫШЕЛ FORGE 1.17');
        }

        let launchType;
        if (gameType === 0) {
            // console.log('single');
            launchType = launch({
                accessToken: userAuth.accessToken,
                clientToken: userAuth.clientToken,
                gameProfile: userAuth.selectedProfile,
                availableProfiles: userAuth.availableProfiles,
                user: userAuth.user,
                gamePath,
                javaPath,
                version: version
            })
        } else if (gameType === 1) {
            // console.log('multi');
            launchType = launch({
                accessToken: userAuth.accessToken,
                clientToken: userAuth.clientToken,
                gameProfile: userAuth.selectedProfile,
                availableProfiles: userAuth.availableProfiles,
                user: userAuth.user,
                server: {
                    ip: arg.ip,
                    port: arg.port,
                },
                gamePath,
                javaPath,
                version: version
            })
        }
        launchType.then(r => {
            event.reply('service-events', {action: 'hide-launching-modal'});
            event.reply('service-events', {action: 'show-launched-modal'});
            wp.lockWhileMcRunning(r.pid).then(() => {
                event.reply('service-events', {action: 'hide-launched-modal'});
            });
        }).catch(e => {
            event.reply('service-events', {action: 'hide-launching-modal'});
            event.reply('service-events', {action: 'hide-launched-modal'});
            event.reply('service-events', {action: 'error-log', data: e});
            customLog.error('Error while launching mc', e);
            console.log(e);
        });

    }

    /**
     * Наполняет и сортирует временное хранилище Forge версий и отсылает в launcherSettings
     * @param arg
     * @param launcherSettings
     */
    saveForgeVer(arg, launcherSettings) {
        launcherSettings.installedForgeVersions.push({
            version: arg.forgeVersion.version,
            mcversion: arg.forgeVersion.mcversion,
        })
        versionSort(launcherSettings.installedForgeVersions, 1);
        fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
    }

    /**
     * Наполняет и сортирует временное хранилище Vanilla версий и отсылает в launcherSettings
     * @param arg
     * @param launcherSettings
     */
    saveVer(arg, launcherSettings) {
        if (!arg.forgeVersion) {
            launcherSettings.installedVersions.push({id: arg.version});
            versionSort(launcherSettings.installedVersions, 0);
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        }
        // Парсинг Forge версий возвражает массив с, отличными от Vanilla версий, ключами. Поэтому здесь написан чуть изменненный код под эту особенность.
        else {
            launcherSettings.installedVersions.push({id: arg.forgeVersion.mcversion});
            versionSort(launcherSettings.installedVersions, 0);
            fs.writeFileSync(launcherSettingsPath, JSON.stringify(launcherSettings), 'utf-8');
        }
    }

    /**
     * Производит установку Forge версии, после чего запускает игру (function launch).
     * @param event
     * @param arg
     * @param gameType - 1 (Server) || 0 (Offline)
     * @param isOnline {boolean}
     * @param launcherSettings
     */
    setupForge(event, arg, gameType, isOnline, launcherSettings) {

        event.reply('service-events', {action: 'show-installing-modal'});

        const versionsPath = mcFolderPath + '\\versions';
        const forgeFolderName = arg.forgeVersion.mcversion + '-forge-' + arg.forgeVersion.version;

        const javaPath = globalJavaPath + '\\0+\\bin\\java.exe';
        console.info('javaPath for FORGE', 'ПОСМОТРИ JAVAPATH, ЕСЛИ УЖЕ ВЫШЕЛ FORGE 1.17');

        fsExtra.copySync(versionsPath + '\\' + arg.forgeVersion.mcversion, versionsPath + '\\' + forgeFolderName);
        console.info('Копирование Vanilla версии в Forge - ЗАВЕРШЕНО')
        console.info('Начата установка Forge')

        // Fake ProgressBar
        let fakeCurrent = 15;
        const fakeTotal = 350;
        const fakeProgressBar = setInterval(() => {
            let randomNum = Math.floor(Math.random() * (Math.random() * 25));
            if (fakeCurrent + randomNum < 350) {
                fakeCurrent += randomNum;
            } else {
                fakeCurrent = 0;
            }
            event.reply('service-events', {
                action: 'set-data-launching-modal',
                progressCurrent: fakeCurrent,
                progressTotal: fakeTotal,
            });
        }, 250)

        installForge({
            version: arg.forgeVersion.version,
            mcversion: arg.forgeVersion.mcversion
        }, mcFolderPath, {java: javaPath}).then(() => {
            event.reply('service-events', {action: 'hide-installing-modal'});
            event.reply('service-events', {action: 'set-data-launching-modal', progressCurrent: fakeCurrent,});
            clearInterval(fakeProgressBar);
            console.info('Установка Forge завершена');


            this.saveForgeVer(arg, launcherSettings);
            this.launch(event, arg, 1, gameType, isOnline, launcherSettings)
        }).catch(e => {
            event.reply('service-events', {action: 'hide-installing-modal'});
            console.error('installForge error', e);
        })
    }

    /**
     * Производит установку* и запуск Vanilla версии, если присутствует аргумент arg.forgeVersion вызывает установку и запуск уже Forge версии.
     * - Установка производится в любом случае, а вот запуск уже в зависимости от аргумента arg.forgeVersion.
     * @param event
     * @param arg
     * @param data - {data} from getVersionList()
     * @param gameType - 1 (Server) || 0 (Offline)
     * @param isOnline {boolean}
     * @param launcherSettings
     */
    setup(event, arg, data, gameType, isOnline, launcherSettings) {
        event.reply('service-events', {action: 'show-installing-modal'});

        // Прогресс бар
        let version;
        if (!arg.forgeVersion) {
            version = data.versions.find(version => version.id === arg.version);
        } else {
            version = data.versions.find(version => version.id === arg.forgeVersion.mcversion);
        }

        const iTask = installTask(version, gamePath);
        iTask.startAndWait({
            onStart(iEvent) {
                // console.debug('iTask starts iEvent', {iTask, iEvent})
            },
            onUpdate(iEvent, chunkSize) {
                // console.debug('iTask update iEvent', {iTask, iEvent});
                event.reply('service-events', {
                    action: 'set-data-launching-modal',
                    progressCurrent: iTask.progress,
                    progressTotal: iTask.total
                });
            },
            onFailed(iEvent, error) {
                console.error('Some error with installing', {iEvent, error});
            },
            onSuccessed(task, result) {
                // console.debug('iTask finnished iEvent', {iTask, task, result});
            }
        });

        // Install
        install(version, gamePath).then(() => {
            event.reply('service-events', {action: 'hide-installing-modal'});


            this.saveVer(arg, launcherSettings);

            if (!arg.forgeVersion) {
                this.launch(event, arg, data, gameType, isOnline, launcherSettings);
            } else {
                this.setupForge(event, arg, gameType, isOnline, launcherSettings);
            }

        }).catch(e => {
            event.reply('service-events', {action: 'hide-installing-modal'});
            event.reply('service-events', {action: 'error-log', data: e});
            customLog.error('Error while installing mc', e);
            console.log(e);
        });

    }

    /**
     * Инициализация проверки для запуска Vanilla версии
     * @param event
     * @param arg
     * @param data - {data} from getVersionList()
     * @param gameType - 1 (server) || 0 (offline)
     * @param isOnline {boolean}
     * @param launcherSettings
     */
    start(event, arg, data, gameType, isOnline, launcherSettings) {

        const isInstalled = launcherSettings.installedVersions.findIndex(version => version.id === arg.version);
        const isInstalled2 = launcherSettings.installedForgeVersions.findIndex(version => version.mcversion === arg.version);

        // console.log('ver', arg.version);
        // console.log('arr', JSON.stringify(launcherSettings.installedVersions));
        // console.log('is', isInstalled);

        if (isInstalled > -1 || isInstalled2 > -1) {
            this.launch(event, arg, data, gameType, isOnline, launcherSettings);
        } else if (isOnline === true) {
            this.setup(event, arg, data, gameType, isOnline, launcherSettings);
        }
    }

    startForge(event, arg, gameType, isOnline, launcherSettings) {
        const requiredVanillaVer = arg.forgeVersion.mcversion;
        const isForgeInstalled = launcherSettings.installedForgeVersions.findIndex(item => item.version === arg.forgeVersion.version);
        const isVanillaInstalled = launcherSettings.installedVersions.findIndex(item => item.id === requiredVanillaVer);
        const isVanillaInstalled2 = launcherSettings.installedForgeVersions.findIndex(item => item.mcversion === requiredVanillaVer);

        // console.debug(requiredVanillaVer);
        // console.debug(launcherSettings.installedVersions);
        // console.debug(isVanillaInstalled);

        if (isForgeInstalled > -1) {
            this.launch(event, arg, 1, 0, isOnline, launcherSettings);
        } else if (isForgeInstalled < 0 && isVanillaInstalled > -1 || isVanillaInstalled2 > -1) {
            console.info('Необходимая версия Vanilla найдена');
            this.setupForge(event, arg, 0, isOnline, launcherSettings)
        } else {
            getVersionList().then((data) => {
                this.setup(event, arg, data, 0, isOnline, launcherSettings);
            }).catch((e) => {
                event.reply('service-events', {action: 'error-log', data: e});
                console.log(e);
            });
        }
    }
}
export default new Game();